'use client'
import { useSession } from 'next-auth/react';
import { useParams } from 'next/navigation';
import React, { useEffect, useState } from 'react'
import {UserProfile} from "../../components/UserProfile"

const page = () => {
  const { userId } = useParams();
  const { data: session } = useSession();

  const [user, setUser] = useState('')

  useEffect(() => {
      getUserData();
  }, [userId]);

  const getUserData = async () => {
    try {
      const response = await fetch('/api/user', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ id:userId }),
      });

      if (response.ok) {
        const userData = await response.json();
        setUser(userData)
      } else {
      }
    } catch (error) {
      console.error(error);
    }
  };
  

  return (
    <div>
      <UserProfile
      user={user}
      />
    </div>
  )
}

export default page