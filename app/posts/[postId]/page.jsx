"use client";
// pages/posts/[postId]/page.jsx
import React, { useState, useEffect } from "react";
import { useParams } from "next/navigation";
import Link from "next/link";
import { useSession } from "next-auth/react";
import BookmarkBorderIcon from "@mui/icons-material/BookmarkBorder";
import BookmarkIcon from "@mui/icons-material/Bookmark";
import BusinessCenterIcon from '@mui/icons-material/BusinessCenter';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import PaidIcon from '@mui/icons-material/Paid';
import Snackbar from "@mui/material/Snackbar";

const PostPage = () => {
  const [post, setPost] = useState(null);
  const { postId } = useParams();
  const { data: session } = useSession();

  const [isFavorited, setIsFavorited] = useState();
  const [isApplied, setIsApplied] = useState();
  const [user, setUser] = useState();
  const [open, setOpen] = useState(false);
  const [authMessage, setAuthMessage] = useState(false);
  const [ownerMessage, setOwnerMessage] = useState('');

  useEffect(() => {
    if (session) {
      getUserData(session.user.email);
    }
  }, [session]);

  const getUserData = async (email) => {
    try {
      const response = await fetch("/api/user", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      });
      if (response.ok) {
        const userData = await response.json();
        setUser(userData);
      } else {
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    if (user && user.favoritePosts && user.favoritePosts.includes(postId)) {
      setIsFavorited(true);
    } else {
      setIsFavorited(false);
    }
    if (post && post.candidates && post.candidates.includes(user.id)) {
      setIsApplied(true);
    } else {
      setIsApplied(false);
    }
  }, [postId, user]);

  const toggleFavorite = async () => {

    if(user){
       const response = await fetch("/api/updateFavs", {
      method: "PUT",
      body: JSON.stringify({
        postId: postId, // Pass the post ID to the API
        isFavorited,
        email: user.email, // Pass the updated favorite status
      }),
    });

    if (response.ok) {
      // Handle successful update
      setOpen(true)
    setIsFavorited(!isFavorited);

      console.log("Favorite posts updated successfully!");
    } else {
      // Handle error during update
      console.error("Error updating favorite posts:", response.statusText);
    }
    }
    else{
      setAuthMessage(true)
    }
    // Update user's favoritePosts on the server-side
   
  };

  const applyToPost = async () => {
    // Send a request to apply to the post
    if (post && user ) {
      if(user.companyId != post.companyId){
           const response = await fetch("/api/apply", {
        method: "PUT",
        body: JSON.stringify({
          postId: postId, // Pass the post ID to the API
          userId: user.id, // Pass the user ID to the API
        }),
      });

      if (response.ok) {
        // Handle successful application
        setOpen(true);
        console.log("Applied to post successfully!");
      } else {
        // Handle error during application
        console.error("Error applying to post:", response.statusText);
      }
      }
      else{
        setOwnerMessage(true)
      }
   
    }
    else{
      setAuthMessage(true)
    }
  };

  useEffect(() => {
    async function fetchPost() {
      try {
        const response = await fetch(`/api/getPostById/${postId}`, {
          method: "GET",
        });
        if (!response.ok) {
          throw new Error("Failed to fetch post");
        }
        const postData = await response.json();
        setPost(postData);
      } catch (error) {
        console.error("Error:", error);
      }
    }

    fetchPost();
  }, [postId]);

  if (!post) {
    return <div>Loading...</div>;
  }

  return (
    <div className="">
      <div className="bg-gray-100">
        <div className="container mx-auto py-8">
          <div className="grid grid-cols-4 sm:grid-cols-12 gap-6 px-4">
            <div className="col-span-4 sm:col-span-3">
              <div className="bg-white shadow rounded-lg p-6">
                <div className="flex flex-col items-center" style={{overflowWrap:"anywhere"}}>
                  <img
                    src={post.user.image}
                    className="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0"
                  />
                  <h1 className="text-xl font-bold">
                    <Link
                      href="/company/[companyId]"
                      as={`/company/${post.companyId}`}
                    >
                      {post.companyName}
                    </Link>
                  </h1>
                  <p className="text-gray-700">{post.user.workArea}</p>
                  <div className="mt-6 flex flex-wrap gap-4 justify-center">
                    <h1>{post.user.email}</h1>
                    <h1>{post.user.phoneNumber}</h1>
                    <h1>{post.user.address}</h1>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-4 sm:col-span-9">
              <div className="bg-white shadow rounded-lg p-6">
                <div className="flex-end">
                  {isFavorited ? (
                    <BookmarkIcon
                      style={{ cursor: "pointer" }}
                      onClick={toggleFavorite}
                    />
                  ) : (
                    <BookmarkBorderIcon
                      style={{ cursor: "pointer" }}
                      onClick={toggleFavorite}
                    />
                  )}
                </div>
                <h2 className="text-xl font-bold mt-4">{post.jobTitle}</h2>
                <p className="text-gray-700 mt-4">
                <BusinessCenterIcon/>{post.workPlace + " - " + post.jobType}
                </p>
                <p className="text-gray-700 mt-4"><LocationOnIcon/>{post.location}</p>
                <h2 className="text-xl font-bold mt-4">Description</h2>
                <p className="text-gray-700">{post.description}</p>
                <h2 className="text-xl font-bold mt-4">Requirements</h2>
                <p className="text-gray-700">{post.requirements}</p>
                <p className="text-gray-700"><PaidIcon/>{post.salaryRange}</p>
              </div>
            </div>
          </div>
        </div>

        <div className="flex-end">
          <button
            onClick={applyToPost}
            type="button"
            disabled={isApplied}
            className="text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 shadow-lg shadow-purple-500/50 dark:shadow-lg dark:shadow-purple-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
          >
            {isApplied ? "Applied" : "Apply"}
          </button>
        </div>
      </div>
      <Snackbar
        open={open}
        autoHideDuration={5000}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        onClose={() => setOpen(false)}
        message="Success"
      />
         <Snackbar
        open={authMessage}
        autoHideDuration={5000}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        onClose={() => setAuthMessage(false)}
        message="Please Log In First"
      />
        <Snackbar
        open={ownerMessage}
        autoHideDuration={5000}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        onClose={() => setOwnerMessage(false)}
        message="You can not apply to your own post."
      />
    </div>
  );
};

export default PostPage;
