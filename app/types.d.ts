type Todo = {
    userId: number,
    id: number,
    title: string,
    completed: boolean,
}

type Student = {
    studentName: string,
    studentNote: string,
}

type User = {
    email: string,
    name: string,
    role:string,
    profession:string,
    phoneNumber:string,
    dob: string,
    address:string,
    aboutMe:string,
    skills:string,
    education:string,
    experience: string,

    companyId: string,
    companyName: string,
    companyDescription: string,
    workArea: string,
    favoritePosts:string[],
    id:string,
}
type Company = {
    companyName: string,
    companyDescription: string,
}

type Post = {
    id:string,
    companyId: string,
    companyName: string,
    jobTitle:string,
    workplace:string,
    location:string,
    jobType:string,
    salaryRange:string,   
    requirements:string,
    description:string,
    createdAt:time,
    companyLogo:string,
    candidates:string[],

}
