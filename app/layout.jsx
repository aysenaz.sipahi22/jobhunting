import '../styles/globals.css'
import Nav from './components/Nav'
import Provider from './components/Provider'

export const metadata = {
    title:"Job Hunter",
    description:"Job finding application"
}

const RootLayout = ({children}) => {
  return (
   <html lang='en'>
        <body>
        <meta name="google-site-verification" content="dP1HDR4Tc0Ss7zJSbjBG8zKAuOtnVvzQqfCZo_l5BFw" />

        <link
            rel="icon"
            href="/images/jobHuntingLogo.png"
            type="image/x-icon"
        />
            <Provider>
            <div className='main'>
                <div className='gradient'></div>
            </div>
            <main className='app'>
                <Nav/>
                {children}
            </main>
            </Provider>            
        </body>
   </html>
  )
}

export default RootLayout