'use client'
import React, { useEffect, useState } from 'react'
import { useSession } from 'next-auth/react'
import PostListCompanyItem from '../components/PostListICompanytem '

const page = () => {
  const [posts, setPosts] = useState([])
  const [user, setUser] = useState('')

  const { data: session } = useSession();

  useEffect(() => {   
    getPostsData()
  }, [user]);

  useEffect(() => {
    if (session) {
      getUserData(session.user.email);
    }
  }, [session]);

  const getUserData = async (email) => {
    try {
      const response = await fetch('/api/user', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email }),
      });
      if (response.ok) {
        const userData = await response.json();
        setUser(userData)

      } else {
      }
    } catch (error) {
      console.error(error);
    }
  };
  
  const getPostsData = async () => {
    try {
      const response = await fetch('/api/getPosts', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-store',
        },
        cache:"no-store",
        

      });
      if (response.ok) {
        const postsData = await response.json();
        // console.log(user)
        const favoritePosts = postsData.filter(post => post && post.companyId && post.companyId.includes(user.companyId));

        // Favori gönderileri setPosts ile güncelle
        setPosts(favoritePosts);
      } else {
      }
    } catch (error) {
      console.error(error);
    }
  };
  
  return (
    <div style={{display:"grid"}}>
      {posts && posts.map((post) => (
        <PostListCompanyItem
        key = {post.id}
        post = {post}
        user = {user}
        />
      )       
      )}
      {(posts.length == "0" || posts == undefined) ? (
        <div>Posts Not Found</div>
      ) :(
        <div></div>
        
      )}
    </div>
  )
}

export default page