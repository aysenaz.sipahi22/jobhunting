'use client'
import React, { useEffect, useState } from 'react'
import  PostListItem  from "../components/PostListItem"
import { useSession } from 'next-auth/react'

const page = () => {
  const [posts, setPosts] = useState([])
  const [user, setUser] = useState('')

  const { data: session } = useSession();

  useEffect(() => {   
    getPostsData()
  }, [user]);

  useEffect(() => {
    if (session) {
      getUserData(session.user.email);
    }
  }, [session]);

  const getUserData = async (email) => {
    try {
      const response = await fetch('/api/user', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email }),
      });
      if (response.ok) {
        const userData = await response.json();
        setUser(userData)

      } else {
      }
    } catch (error) {
      console.error(error);
    }
  };
  
  const getPostsData = async () => {
    try {
      const response = await fetch('/api/getPosts', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-store',
        },
        cache:"no-store",

      });
      if (response.ok) {
        const postsData = await response.json();
        // console.log(postsData,"bıvdfk")
        // console.log(user)
        const favoritePosts = postsData.filter(post => post && post.candidates && post.candidates.includes(user.id));

        // Favori gönderileri setPosts ile güncelle
        setPosts(favoritePosts);
      } else {
      }
    } catch (error) {
      console.error(error);
    }
  };
  
  return (
    <div style={{display:"grid"}}>
      {posts && posts.map((post) => (
        <PostListItem
        key = {post.id}
        post = {post}
        user = {user}
        />
      )       
      )}
    </div>
  )
}

export default page