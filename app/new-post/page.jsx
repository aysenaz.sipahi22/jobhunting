"use client";

import { useState } from "react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import Form from "../components/Form"


const newPost = () => {
    const router = useRouter();
    const { data: session } = useSession();
  
    const [submitting, setIsSubmitting] = useState(false);
    const [post, setPost] = useState();
  
    const createPrompt = async (e) => {
      e.preventDefault();
      setIsSubmitting(true);
      try {
        const response = await fetch("/api/addPost", {
          method: "POST",
          body: JSON.stringify({
            userId: session?.user.id,
            jobTitle: post.jobTitle,
            companyName: post.company,
            workplace: post.workplace,
            location: post.location,
            jobType: post.jobType,
            salaryRange: post.salaryRange,
            companyId: post.companyId,
            description:post.description,
            requirements: post.requirements,
            companyLogo: session?.user.image,
            
          }),
        });
  
        if (response.ok) {
          router.push("/");
        }
      } catch (error) {
        console.log(error);
      } finally {
        setIsSubmitting(false);
      }
    };
  
    return (
      <Form
        type='Create'
        post={post}
        setPost={setPost}
        submitting={submitting}
        handleSubmit={createPrompt}
      />
    );
  };

export default newPost