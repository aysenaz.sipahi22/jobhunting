"use client";
// pages/posts/[postId]/page.jsx
import React, { useState, useEffect } from "react";
import { useParams } from "next/navigation";
import Link from "next/link";
import CandidateListItem from "../../components/CandidateListItem"
import CandidatesList from "../../components/CandidatesList"

const CandidatePage = () => {
  const [post, setPost] = useState(null);
  const [candidates, setCandidates] = useState([]);
  const { postId } = useParams();
  const [users, setUsers] = useState([])

  useEffect(() => {
    async function fetchPost() {
      try {
        const response = await fetch(`/api/getPostById/${postId}`, {
          method: "GET",
        });
        if (!response.ok) {
          throw new Error("Failed to fetch post");
        }
        const postData = await response.json();
        setPost(postData);
        setCandidates(postData.candidates)
      } catch (error) {
        console.error("Error:", error);
      }

      
    }

    fetchPost();
  }, [postId]);

  useEffect(() => {
    const getUserData = async () => {
      const userDataPromises = candidates && candidates.map(async (candidate) => {
        try {
          const response = await fetch('/api/user', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({ id: candidate }),
          });
          if (!response.ok) {
            throw new Error('Failed to fetch user data');
          }
          return response.json();
        } catch (error) {
          console.error("Error fetching user data:", error);
          return null; // Hata durumunda null döndür
        }
      });
  
      try {
        const userDataArray = await Promise.all(userDataPromises);
        setUsers(userDataArray.filter(userData => userData !== null)); // Null olanları filtrele
      } catch (error) {
        console.error("Error fetching user data:", error);
        setUsers([]); // Hata durumunda boş dizi set et
      }
    };
  
    getUserData();
  }, [candidates]);
  
  

  if (!post) {
    return <div>Loading...</div>;
  }

  return (
    <div>
    {/* <input
     type="text"
     value={searchTerm}
     onChange={(e) => setSearchTerm(e.target.value)}
     placeholder="Search..."
     className="px-4 py-2 border rounded-md w-72 focus:outline-none focus:ring-2 focus:ring-purple-500"
   /> */}
    <div style={{display:"grid"}}>
        <CandidatesList candidates={users}/>
  
 </div>
 </div>
  );
};

export default CandidatePage;
