import { MongoClient } from "mongodb";

const url = "mongodb+srv://nazsph:naz123@cluster0.ucqal4p.mongodb.net/?retryWrites=true&w=majority";
const database = "posts";

export async function PUT(request) {
  const { postId, userId } = await request.json();

  const client = new MongoClient(url);
  try {
    await client.connect();
    const db = client.db(database);
    const postsCollection = db.collection("posts");

    // Find the post based on its ID
    const post = await postsCollection.findOne({ id: postId });

    if (post) {
      // Check if the user has already applied to this post
      if (post.candidates && post.candidates.includes(userId)) {
        return new Response("User already applied to this post", { status: 400 });
      }

      // Add the user ID to the post's candidates array
      const updatedCandidates = post.candidates ? [...post.candidates, userId] : [userId];
      // console.log(updatedCandidates)
      const result = await postsCollection.updateOne(
        { id: postId }, // Update the post document by its ID
        { $set: { candidates: updatedCandidates } }
      );

      if (result.modifiedCount === 1) {
        // Update successful
        return new Response("Applied to post successfully", { status: 200 });
      } else {
        // Update failed (e.g., post not found)
        return new Response("Error applying to post", { status: 404 });
      }
    } else {
      // Post not found
      return new Response("Post not found", { status: 404 });
    }
  } catch (error) {
    console.error("Error applying to post:", error);
    return new Response("An error occurred", { status: 500 });
  } finally {
    await client.close();
  }
}
