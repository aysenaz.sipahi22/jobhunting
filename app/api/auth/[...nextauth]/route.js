// import User from '@app/models/user';
import  User  from "../../../models/user"
// import { connectToDB } from '@utils/database';
import {connectToDB} from "../../../../utils/database"
import NextAuth from 'next-auth/next';
import GoogleProvider from 'next-auth/providers/google';

function generateRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

const handler = NextAuth({
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    })
  ],
  callbacks: {
   async session({ session }) {
  // store the user id from MongoDB to session
  const sessionUser = await User.findOne({ email: session.user.email });
  if (sessionUser) {
    session.user.id = sessionUser._id.toString();
  }
  return session;
},
    async signIn({ account, profile, user, credentials }) {
      try {
        await connectToDB();

        // check if user already exists
        const userExists = await User.findOne({ email: profile.email });

        // if not, create a new document and save user in MongoDB
        if (!userExists) {
          await User.create({
            id: generateRandomNumber(100000, 999999).toString(),
            email: profile.email,
            username: profile.name.replace(" ", "").toLowerCase(),
            image: profile.picture,
            companyId: generateRandomNumber(100000, 999999).toString(),

          });
        }

    
        return true
      } catch (error) {
        console.log("Error checking if user exists: ", error.message);
        return false
      }
    },
  }
})

export { handler as GET, handler as POST }