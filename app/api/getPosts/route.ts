import { NextResponse } from "next/server"
import { MongoClient } from 'mongodb'


  const url = "mongodb+srv://nazsph:naz123@cluster0.ucqal4p.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0"
  const database = "posts"


  export async function GET(request: Request) {
    const client = new MongoClient(url);
    try {
      await client.connect();
      const db = client.db(database);
      const postsCollection = db.collection('posts');
  
      const posts = await postsCollection.find({}).sort({ createdAt: -1 }).toArray();
  
      // Yanıta Cache-Control başlığını ekleyin
      const response = NextResponse.json(posts);
      response.headers.set('Cache-Control', 'no-store');
  
      return response;
    } catch (error) {
      console.error('Error:', error);
      return new Response('An error occurred while processing your request', { status: 500 });
    } finally {
      await client.close();
    }
  }