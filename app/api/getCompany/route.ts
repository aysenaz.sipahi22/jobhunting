import { NextResponse } from "next/server"
import { MongoClient } from 'mongodb'


  const url = "mongodb+srv://nazsph:naz123@cluster0.ucqal4p.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0"
  const database = "company"


  export async function GET(request: Request) {
    const client = new MongoClient(url);
    try {
      await client.connect();
      const db = client.db(database);
      const studentsCollection = db.collection("companies");
  
      const students = await studentsCollection.find({}).toArray();
      return NextResponse.json(students);
    } catch (error) {
      console.error("Error:", error);
      return new Response("An error occurred while processing your request", { status: 500 });
    } finally {
      await client.close();
    }
  }