import { NextResponse } from "next/server"
import { MongoClient } from 'mongodb'


  const url = "mongodb+srv://nazsph:naz123@cluster0.ucqal4p.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0"
  const database = "user"

  function generateRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }


  export async function PUT(request: Request) {
    const { email, name,role,id,profession,phoneNumber,dob,address,aboutMe,skills,education,experience, companyName,companyId, companyDescription,workArea }: Partial<User> = await request.json();
  
    const client = new MongoClient(url);
    try {
      await client.connect();
      const db = client.db(database);
      const usersCollection = db.collection("users");
  
      const result = await usersCollection.updateOne(
        { email: email },
        {
          $set: {
            id:id ? id.toString() : generateRandomNumber(100000,999999).toString(),
            role:role,
            name: name,
            profession:profession,
            phoneNumber:phoneNumber,
            dob:dob,
            address:address,
            aboutMe:aboutMe,
            skills:skills,
            education:education,
            experience:experience,
            companyName: companyName,
            companyDescription: companyDescription,
            workArea: workArea,
            companyId: companyId ? companyId.toString() : generateRandomNumber(100000,999999).toString(),
          },
        }
      );
  
      if (result.modifiedCount === 1) {
        // Güncelleme başarılı
        return new Response("Kullanıcı bilgileri güncellendi", { status: 200 });
      } else {
        // Kullanıcı bulunamadı veya güncelleme yapılmadı
        return new Response("Kullanıcı bulunamadı veya güncelleme yapılamadı", { status: 404 });
      }
    } catch (error) {
      console.error("Error:", error);
      return new Response("Bir hata oluştu, kullanıcı bilgileri güncellenemedi", { status: 500 });
    } finally {
      await client.close();
    }
  }
  