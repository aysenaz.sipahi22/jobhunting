// /api/getPostById/[postId].ts
import { MongoClient, ObjectId } from 'mongodb';
import { NextResponse } from "next/server"

const url = "mongodb+srv://nazsph:naz123@cluster0.ucqal4p.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";
const database = "posts";

export  async function GET(req: Request) {
    const  postId  = (req.url.split('/')[5]).toString()

    // postId'nin undefined olup olmadığını kontrol et
    if (!postId) {
        return NextResponse.json({ message: "postId parameter is required" });
    }

    const client = new MongoClient(url);

    try {
        await client.connect();
        const db = client.db(database);
        const postsCollection = db.collection("posts");

        // ObjectId olarak dönüştür

        const post = await postsCollection.findOne({ id: postId });

        if (!post) {
            return NextResponse.json({ message: "Post not found" });
        }

        return NextResponse.json(post);
    } catch (error) {
        console.error("Error:", error);
        return NextResponse.json({ message: "An error occurred while processing your request" });
    } finally {
        await client.close();
    }
}
