// /api/getPostById/[postId].ts
import { MongoClient, ObjectId } from 'mongodb';
import { NextResponse } from "next/server"

const url = "mongodb+srv://nazsph:naz123@cluster0.ucqal4p.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";
const database = "user";

export  async function GET(req: Request) {
    const  companyId  = (req.url.split('/')[5]).toString()
    // console.log("56789",companyId,req.url)
    // postId'nin undefined olup olmadığını kontrol et
    if (!companyId) {
        return NextResponse.json({ message: "postId parameter is required" });
    }

    const client = new MongoClient(url);

    try {
        await client.connect();
        const db = client.db(database);
        const userCollection = db.collection("users");

        // ObjectId olarak dönüştür

        const company = await userCollection.findOne({ companyId: companyId });

        if (!company) {
            return NextResponse.json({ message: "Post not found" });
        }

        return NextResponse.json(company);
    } catch (error) {
        console.error("Error:", error);
        return NextResponse.json({ message: "An error occurred while processing your request" });
    } finally {
        await client.close();
    }
}
