import { MongoClient } from 'mongodb';
import { NextResponse } from "next/server"

const url = "mongodb+srv://nazsph:naz123@cluster0.ucqal4p.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";
const database = "posts";

function generateRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }


export async function POST(request: Request) {
  const { companyId, companyName, jobTitle, jobType, location, workplace, salaryRange,requirements,description,companyLogo }: Partial<Post> = await request.json();
  const client = new MongoClient(url);
  try {
    await client.connect();
    const db = client.db(database);
    const postsCollection = db.collection("posts");

    const dbUser = client.db("user");
    const usersCollection = dbUser.collection("users");
    const user = await usersCollection.findOne({ companyId: companyId });

    // console.log(companyId, companyName, jobTitle, jobType, location, workplace, salaryRange,companyLogo)
    const result = await postsCollection.insertOne({
        id: generateRandomNumber(100000, 999999).toString(),
        companyName: companyName,
      companyId: companyId,
      jobTitle: jobTitle,
      jobType: jobType,
      location: location,
      workPlace: workplace,
      salaryRange: salaryRange,
      requirements: requirements,
      description: description,
      createdAt: new Date(),
      companyLogo: companyLogo,
      user:user,
    });
    if (result) {
      return NextResponse.json(result);
    } else {
      throw new Error("Failed to add post");
    }
  } catch (error) {}
  return new Response("aaaa");
}