import { NextResponse } from "next/server";
import { MongoClient } from "mongodb";

const url = "mongodb+srv://nazsph:naz123@cluster0.ucqal4p.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";
const database = "user";

export async function PUT(request) {
  const { postId, isFavorited, email } = await request.json();

  const client = new MongoClient(url);
  try {
    await client.connect();
    const db = client.db(database);
    const usersCollection = db.collection("users");

    // Find the user based on their session data (e.g., email)
    const user = await usersCollection.findOne({ email: email });

    if (user) {
      let updatedFavoritePosts;
      if (isFavorited) {
        // Add the post ID to the user's favoritePosts array
        updatedFavoritePosts = user.favoritePosts.filter((id) => id !== postId);
      } else {
        // Remove the post ID from the user's favoritePosts array
        updatedFavoritePosts = user.favoritePosts ? [...user.favoritePosts, postId] : [postId];

      }

      const result = await usersCollection.updateOne(
        { _id: user._id }, // Update the user document by their ID
        { $set: { favoritePosts: updatedFavoritePosts } }
      );

      if (result.modifiedCount === 1) {
        // Update successful
        return new Response("Favorite posts updated successfully", { status: 200 });
      } else {
        // Update failed (e.g., user not found)
        return new Response("Error updating favorite posts", { status: 404 });
      }
    } else {
      // User not found
      return new Response("User not found", { status: 404 });
    }
  } catch (error) {
    console.error("Error updating favorite posts:", error);
    return new Response("An error occurred", { status: 500 });
  } finally {
    await client.close();
  }
}
