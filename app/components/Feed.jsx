'use client'
import React, { useEffect, useState } from 'react'
import  PostListItem  from "./PostListItem"
import { useSession } from 'next-auth/react'

const Feed = () => {
  const [posts, setPosts] = useState([])
  const [user, setUser] = useState('')
  const [searchTerm, setSearchTerm] = useState('')

  const { data: session } = useSession();

  useEffect(() => {
    // if (session) {
    //   getUserData(session.user.email);
    // }
    getPostsData()
  }, []);


  useEffect(() => {
    if (session) {
      getUserData(session.user.email);
    }
  }, [session]);

  const getUserData = async (email) => {
    try {
      const response = await fetch('/api/user', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email }),
      });
      if (response.ok) {
        const userData = await response.json();
        setUser(userData)

      } else {
      }
    } catch (error) {
      console.error(error);
    }
  };
  
  const getPostsData = async () => {
    try {
      const response = await fetch('/api/getPosts', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-store',
        },
        cache:"no-store",

      });
      if (response.ok) {
        const postsData = await response.json();
        setPosts(postsData)
  
      } else {
      }
    } catch (error) {
      console.error(error);
    }
  };
  
  return (
    <div>
       {/* <input
        type="text"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
        placeholder="Search..."
        className="px-4 py-2 border rounded-md w-72 focus:outline-none focus:ring-2 focus:ring-purple-500"
      /> */}
       <div style={{display:"grid"}}>
      {posts && posts.map((post) => (
        <PostListItem
        key = {post.id}
        post = {post}
        user = {user}
        />
      )       
      )}
    </div>
    </div>
   
  )
}

export default Feed