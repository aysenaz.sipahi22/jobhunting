'use client'
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import BusinessCenterIcon from '@mui/icons-material/BusinessCenter';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import PaidIcon from '@mui/icons-material/Paid';
import DeleteIcon from '@mui/icons-material/Delete';
import Snackbar from "@mui/material/Snackbar";

const PostListCompanyItem = ({ post,user }) => {
  const { data: session } = useSession();
  const [isFavorited, setIsFavorited] = useState();
  const [isApplied, setIsApplied] = useState();
  const [open, setOpen] = useState(false);
  const [authMessage, setAuthMessage] = useState(false);

  useEffect(() => {
    if (user && user.favoritePosts && user.favoritePosts.includes(post.id)) {
      setIsFavorited(true);
    } else {
      setIsFavorited(false);
    }
    if (post && post.candidates && post.candidates.includes(user.id)) {
      setIsApplied(true);
    } else {
      setIsApplied(false);
    }
  }, [post.id, user]);
  
  
  const toggleFavorite = async () => {

    if(user){
       const response = await fetch("/api/updateFavs", {
      method: "PUT",
      body: JSON.stringify({
        postId: postId, // Pass the post ID to the API
        isFavorited,
        email: user.email, // Pass the updated favorite status
      }),
    });

    if (response.ok) {
      // Handle successful update
      setOpen(true)
    setIsFavorited(!isFavorited);

      console.log("Favorite posts updated successfully!");
    } else {
      // Handle error during update
      console.error("Error updating favorite posts:", response.statusText);
    }
    }
    else{
      setAuthMessage(true)
    }
    // Update user's favoritePosts on the server-side
   
  };

  const deletePost = async () => {

    // Send a request to apply to the post
    if(user){
      
      const response = await fetch("/api/deletePost", {
      method: "DELETE",
      body: JSON.stringify({
        id: post.id, // Pass the post ID to the API       
      }),
    });

    if (response.ok) {
      // Handle successful application
      console.log("post deleted successfully!");
      setOpen(true)
    } else {
      // Handle error during application
      console.error("Error applying to post:", response.statusText);
    }
    }
    else{
      setAuthMessage(true)
    }
    
  };

  return (
    <div className="relative flex flex-col text-gray-700 bg-white shadow-md w-[600px] rounded-xl bg-clip-border mb-10">
      {console.log(post)}
      <nav className="flex min-w-[240px] flex-col gap-1 p-2 font-sans text-base font-normal text-blue-gray-700">
        <div className="flex items-center w-full p-3 leading-tight transition-all rounded-lg outline-none text-start hover:bg-blue-gray-50 hover:bg-opacity-80 hover:text-blue-gray-900 focus:bg-blue-gray-50 focus:bg-opacity-80 focus:text-blue-gray-900 active:bg-blue-gray-50 active:bg-opacity-80 active:text-blue-gray-900">
          <div className="flex-end">
          
          </div>
          <div className="grid mr-4 place-items-center">
            <Link href="/company/[companyId]" as={`/company/${post.companyId}`}>
              <img
                alt="candice"
                src={post.user.image}
                className="relative inline-block h-12 w-12 !rounded-full  object-cover object-center"
              />
            </Link>
          </div>
          <div className=" w-11/12">
            <div className="flex-between">
              <h6 className="block font-sans text-base antialiased font-semibold leading-relaxed tracking-normal text-blue-gray-900">
                {post.jobTitle}
              </h6>
              <div className="flex-end">
                    <button onClick={deletePost}>
            <DeleteIcon/>
          </button>
              {isFavorited ? (
                <BookmarkIcon
                  style={{ cursor: "pointer" }}
                  onClick={toggleFavorite}
                />
              ) : (
                <BookmarkBorderIcon
                  style={{ cursor: "pointer" }}
                  onClick={toggleFavorite}
                />
              )}
              </div>
          
            </div>
            <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
              <Link
                href="/company/[companyId]"
                as={`/company/${post.companyId}`}
              >
                {post.companyName}
              </Link>
            </p>
            <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
              <LocationOnIcon />
              {post.location}
            </p>
            <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
              <BusinessCenterIcon />
              {post.workPlace + " - " + post.jobType}
            </p>
            <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
              {post.salaryRange != "" ? <PaidIcon /> : ""}
              {post.salaryRange}
            </p>
          </div>
        </div>
        <div className="flex-end">
          <Link
            href="/posts/[postId]"
            as={`/posts/${post.id}`}
            className="text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 shadow-lg shadow-purple-500/50 dark:shadow-lg dark:shadow-purple-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
          >
            See Details
          </Link>   
          <Link
            href="/candidates/[postId]"
            as={`/candidates/${post.id}`}
            className="text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 shadow-lg shadow-purple-500/50 dark:shadow-lg dark:shadow-purple-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
          >
            View Candidates
          </Link>      
        </div>
      </nav>
      <Snackbar
        open={open}
        autoHideDuration={5000}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        onClose={() => setOpen(false)}
        message="Success"
      />
         <Snackbar
        open={authMessage}
        autoHideDuration={5000}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        onClose={() => setAuthMessage(false)}
        message="Please Log In First"
      />
    </div>
  );
};

export default PostListCompanyItem;
