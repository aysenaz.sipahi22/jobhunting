"use client";
import React, { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import Snackbar from '@mui/material/Snackbar';
const Profile = () => {
  const { data: session } = useSession();
  const [id, setId] = useState("");
  const [role, setRole] = useState("jobSeeker");
  const [user, setUser] = useState(null);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [profession, setProfession] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [dob, setDob] = useState("");
  const [address, setAddress] = useState("");
  const [aboutMe, setAboutMe] = useState("");
  const [skills, setSkills] = useState("");
  const [education, setEducation] = useState("");
  const [experience, setExperience] = useState("");

  const [workArea, setWorkArea] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [companyId, setCompanyId] = useState("");
  const [companyDescription, setCompanyDescription] = useState("");

  const [open, setOpen] = useState(false) 

  useEffect(() => {
    if (session) {
      getUserData(session.user.email);
    }
  }, [session]);

  useEffect(() => {
    if (user) {
      setRole(user.role);
      setId(user.id);
      setName(session.user.name);
      setEmail(user.email);
      setProfession(user.profession);
      setPhoneNumber(user.phoneNumber);
      setDob(user.dob);
      setAddress(user.address);
      setAboutMe(user.aboutMe);
      setSkills(user.skills);
      setEducation(user.education);
      setExperience(user.experience);

      setWorkArea(user.workArea);
      setCompanyName(user.companyName);
      setCompanyId(user.companyId);
      setCompanyDescription(user.companyDescription);
    }
  }, [user]);

  const getUserData = async (email) => {
    try {
      const response = await fetch("/api/user", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      });
      if (response.ok) {
        const userData = await response.json();
        setUser(userData);
      } else {
        setUser("");
      }
    } catch (error) {
      console.error(error);
      setUser("");
    }
  };
  const handleSave = async () => {
    try {
      const response = await fetch("/api/user/update", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: session.user.email,
          name,
          role,
          id,
          profession,
          phoneNumber,
          dob,
          address,
          aboutMe,
          skills,
          education,
          experience,
          companyName,
          companyId,
          companyDescription,
          workArea,
          
        }),
      });
      if (response.ok) {
        getUserData(session.user.email);
        setOpen(true)
      } else {
        console.error("Bir hata oluştu, bilgiler güncellenemedi.");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleFileChange = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  const handleCheckboxChange = () => {
    // Checkbox'a tıklandığında role durumunu güncelle
    setRole((prevRole) =>
      prevRole === "jobSeeker" ? "employer" : "jobSeeker"
    );
  };
  return (
    <div className="">
      
      {user ? (
        
        <div class="bg-gray-100">
          <label class="inline-flex items-center me-5 cursor-pointer">
        <input
          type="checkbox"
          value=""
          class="sr-only peer"
          checked={role === "employer"} // Checkbox'un checked özelliğini role'a göre belirle
          onChange={handleCheckboxChange} // Checkbox değiştiğinde handleCheckboxChange fonksiyonunu çağır
        />
        <div class="relative w-11 h-6 bg-purple-400 rounded-full peer dark:bg-gray-700 peer-focus:ring-4 peer-focus:ring-purple-300 dark:peer-focus:ring-purple-800 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-purple-600"></div>
        <span class="ms-3 text-sm font-medium text-gray-900 dark:text-gray-300">
          {role === "employer" ? "Employer" : "Job Seeker"}{" "}
          {/* Role'a göre metni dinamik olarak belirle */}
        </span>
      </label>
          {role=="jobSeeker" ? (
            <div class="container mx-auto py-8">
            <div class="grid grid-cols-4 sm:grid-cols-12 gap-6 px-4">
              <div class="col-span-4 sm:col-span-3">
                <div class="bg-white shadow rounded-lg p-6">
                  <div class="flex flex-col items-center">
                    <img
                      src={session?.user.image}
                      class="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0"
                    ></img>
                    <h1 class="text-xl font-bold">{name}</h1>
                    <div class="mb-1">
                      <label
                        for="name"
                        class="mb-3 block text-base font-medium text-[#07074D] text-center"
                      >
                        Profession
                      </label>
                      <input
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Profession"
                        value={profession}
                        onChange={(e) => setProfession(e.target.value)}
                        class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                      />
                    </div>
                    <div class="mt-6 flex flex-wrap gap-4 justify-center">
                      {email}
                      <div class="mb-5">
                        <label
                          for="name"
                          class="mb-3 block text-base font-medium text-[#07074D] text-center"
                        >
                          Phone Number
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          placeholder="Phone Number"
                          value={phoneNumber}
                          onChange={(e) => setPhoneNumber(e.target.value)}
                          class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                      </div>
                      <div class="mb-5">
                        <label
                          for="name"
                          class="mb-3 block text-base font-medium text-[#07074D] text-center"
                        >
                          Date of Birth
                        </label>
                        <input
                          type="date"
                          name="name"
                          id="name"
                          value={dob}
                          onChange={(e) => setDob(e.target.value)}
                          class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                      </div>
                    </div>
                  </div>
                  <div class="flex flex-col">
                    {/* <span class="text-gray-700 uppercase font-bold tracking-wider mb-2">
                      Skills
                    </span>
                    <ul>
                      <li class="mb-2">JavaScript</li>
                      <li class="mb-2">React</li>
                      <li class="mb-2">Node.js</li>
                      <li class="mb-2">HTML/CSS</li>
                      <li class="mb-2">Tailwind Css</li>
                    </ul> */}
                    <label
                      for="name"
                      class="mb-3 block text-base font-medium text-[#07074D] text-center"
                    >
                      Address
                    </label>{" "}
                    <div class="w-full md:w-full px-3 mb-6">
                      <textarea
                        class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md resize-none"
                        required
                        value={address}
                        onChange={(e) => setAddress(e.target.value)}
                      ></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-span-4 sm:col-span-9">
                <div class="bg-white shadow rounded-lg p-6">
                  <h2 class="text-xl font-bold mb-4">About Me</h2>
                  <div class="w-full md:w-full px-3 mb-6">
                    <textarea
                      class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md resize-none"
                      required
                      value={aboutMe}
                      onChange={(e) => setAboutMe(e.target.value)}
                    ></textarea>
                  </div>
                  <h2 class="text-xl font-bold mb-4">Skills</h2>
                  <div class="w-full md:w-full px-3 mb-6">
                    <textarea
                      class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md resize-none"
                      required
                      value={skills}
                      onChange={(e) => setSkills(e.target.value)}
                    ></textarea>
                  </div>
                  <h2 class="text-xl font-bold mb-4">Education</h2>
                  <div class="w-full md:w-full px-3 mb-6">
                    <textarea
                      class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md resize-none"
                      required
                      value={education}
                      onChange={(e) => setEducation(e.target.value)}
                    ></textarea>
                  </div>
                  <h2 class="text-xl font-bold mb-4">Experience</h2>
                  <div class="w-full md:w-full px-3 mb-6">
                    <textarea
                      class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md resize-none"
                      required
                      value={experience}
                      onChange={(e) => setExperience(e.target.value)}
                    ></textarea>
                  </div>

                  {/* <h2 class="text-xl font-bold mt-6 mb-4">Experience</h2>
                  <div class="mb-6">
                    <div class="flex justify-between flex-wrap gap-2 w-full">
                      <span class="text-gray-700 font-bold">Web Developer</span>
                      <p>
                        <span class="text-gray-700 mr-2">at ABC Company</span>
                        <span class="text-gray-700">2017 - 2019</span>
                      </p>
                    </div>
                    <p class="mt-2">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Sed finibus est vitae tortor ullamcorper, ut vestibulum
                      velit convallis. Aenean posuere risus non velit egestas
                      suscipit.
                    </p>
                  </div>
                  <div class="mb-6">
                    <div class="flex justify-between flex-wrap gap-2 w-full">
                      <span class="text-gray-700 font-bold">Web Developer</span>
                      <p>
                        <span class="text-gray-700 mr-2">at ABC Company</span>
                        <span class="text-gray-700">2017 - 2019</span>
                      </p>
                    </div>
                    <p class="mt-2">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Sed finibus est vitae tortor ullamcorper, ut vestibulum
                      velit convallis. Aenean posuere risus non velit egestas
                      suscipit.
                    </p>
                  </div>
                  <div class="mb-6">
                    <div class="flex justify-between flex-wrap gap-2 w-full">
                      <span class="text-gray-700 font-bold">Web Developer</span>
                      <p>
                        <span class="text-gray-700 mr-2">at ABC Company</span>
                        <span class="text-gray-700">2017 - 2019</span>
                      </p>
                    </div>
                    <p class="mt-2">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Sed finibus est vitae tortor ullamcorper, ut vestibulum
                      velit convallis. Aenean posuere risus non velit egestas
                      suscipit.
                    </p>
                  </div> */}
                </div>
              </div>
            </div>
          </div>
          ) : (
            <div>
              <div class="container mx-auto py-8">
            <div class="grid grid-cols-4 sm:grid-cols-12 gap-6 px-4">
              <div class="col-span-4 sm:col-span-3">
                <div class="bg-white shadow rounded-lg p-6">
                  <div class="flex flex-col items-center">
                    <img
                      src={session?.user.image}
                      class="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0"
                    ></img>
                    <div class="mb-1">
                      <label
                        for="name"
                        class="mb-3 block text-base font-medium text-[#07074D] text-center"
                      >
                        Company Name
                      </label>
                      <input
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Company Name"
                        value={companyName}
                        onChange={(e) => setCompanyName(e.target.value)}
                        class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                      />
                    </div>         
                     <div class="mb-1">
                      <label
                        for="name"
                        class="mb-3 block text-base font-medium text-[#07074D] text-center"
                      >
                        Work Area
                      </label>
                      <input
                        type="text"
                        name="name"
                        id="name"
                        placeholder="Work Area"
                        value={workArea}
                        onChange={(e) => setWorkArea(e.target.value)}
                        class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                      />
                    </div>
                    <div class="mt-6 flex flex-wrap gap-4 justify-center">
                      {email}
                      <div class="mb-5">
                        <label
                          for="name"
                          class="mb-3 block text-base font-medium text-[#07074D] text-center"
                        >
                          Phone Number
                        </label>
                        <input
                          type="text"
                          name="name"
                          id="name"
                          placeholder="Phone Number"
                          value={phoneNumber}
                          onChange={(e) => setPhoneNumber(e.target.value)}
                          class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md"
                        />
                      </div>
                      
                    </div>
                  </div>
              
                </div>
              </div>
              <div class="col-span-4 sm:col-span-9">
                <div class="bg-white shadow rounded-lg p-6">
                  <h2 class="text-xl font-bold mb-4">About Us</h2>
                  <div class="w-full md:w-full px-3 mb-6">
                    <textarea
                      class="w-full h-80 rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md resize-none"
                      required
                      value={companyDescription}
                      onChange={(e) => setCompanyDescription(e.target.value)}
                    ></textarea>
                  </div>
                  <div class="flex flex-col">
                   
                   <label
                     for="name"
                     class="mb-3 block text-base font-medium text-[#07074D] text-center"
                   >
                     Address
                   </label>{" "}
                   <div class="w-full md:w-full px-3 mb-6">
                     <textarea
                       class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md resize-none"
                       required
                       value={address}
                       onChange={(e) => setAddress(e.target.value)}
                     ></textarea>
                   </div>
                 </div>
                  {/* <h2 class="text-xl font-bold mb-4">Skills</h2>
                  <div class="w-full md:w-full px-3 mb-6">
                    <textarea
                      class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md resize-none"
                      required
                      value={skills}
                      onChange={() => setSkills(e.target.value)}
                    ></textarea>
                  </div>
                  <h2 class="text-xl font-bold mb-4">Education</h2>
                  <div class="w-full md:w-full px-3 mb-6">
                    <textarea
                      class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md resize-none"
                      required
                      value={education}
                      onChange={() => setEducation(e.target.value)}
                    ></textarea>
                  </div>
                  <h2 class="text-xl font-bold mb-4">Experience</h2>
                  <div class="w-full md:w-full px-3 mb-6">
                    <textarea
                      class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md resize-none"
                      required
                      value={experience}
                      onChange={() => setExperience(e.target.value)}
                    ></textarea>
                  </div> */}

                 
                </div>
              </div>
            </div>
          </div>
            </div>
          )}
          <div className="flex-end mt-5">
          <button
            onClick={() => handleSave()}
            type="button"
            className="text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 shadow-lg shadow-purple-500/50 dark:shadow-lg dark:shadow-purple-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
          >
            Save
          </button>
        </div>
        </div>
      ) : (
        <p>Bilgiler yükleniyor...</p>
      )}
         <Snackbar
        open={open}
        autoHideDuration={5000}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        onClose={() => setOpen(false)}
        message="Success"
      />
    </div>
  );
};

export default Profile;
