import { useEffect, useState } from 'react';
import Link from 'next/link';
import { useSession } from 'next-auth/react'

const Form = ({ type, post, setPost, submitting, handleSubmit }) => {
  const { data: session } = useSession();

  const [jobTitle, setJobTitle] = useState('');
  const [user, setUser] = useState('')
  const [company, setCompany] = useState('');
  const [workplace, setWorkplace] = useState('');
  const [location, setLocation] = useState('');
  const [jobType, setJobType] = useState('');
  const [requirements, setRequirements] = useState('');
  const [description, setDescription] = useState('');
  const [salaryRange, setSalaryRange] = useState('');

  const handleJobTitleChange = (e) => {
    setJobTitle(e.target.value);
    setPost({ ...post, jobTitle: e.target.value });
  };

  const handleCompanyChange = (e) => {
    setCompany(e.target.value);
    setPost({ ...post, company: e.target.value });
  };

  const handleWorkplaceChange = (e) => {
    setWorkplace(e.target.value);
    setPost({ ...post, workplace: e.target.value });
  };

  const handleLocationChange = (e) => {
    setLocation(e.target.value);
    setPost({ ...post, location: e.target.value });
  };

  const handleJobTypeChange = (e) => {
    setJobType(e.target.value);
    setPost({ ...post, jobType: e.target.value });
  };

  const handleRequirementsChange = (e) => {
    setRequirements(e.target.value);
    setPost({ ...post, requirements: e.target.value });
  };

  const handleDescriptionChange = (e) => {
    setDescription(e.target.value);
    setPost({ ...post, description: e.target.value });
  };

  const handleSalaryRangeChange = (e) => {
    setSalaryRange(e.target.value);
    setPost({ ...post, salaryRange: e.target.value });
  };

  useEffect(() => {
    if (session) {
      getUserData(session.user.email);
    }
  }, [session]);

  const getUserData = async (email) => {
    try {
      const response = await fetch('/api/user', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email }),
      });
      if (response.ok) {
        const userData = await response.json();
        setPost({ ...post, "companyId": userData.companyId });
        setUser(userData)
        setCompany(userData.companyName);

      } else {
      }
    } catch (error) {
      console.error(error);
    }
  };
  
  useEffect(() => {
    setPost({ ...post, company: company });
 
  }, [user])
  

  return (
    <section className="w-full max-w-full flex-center flex-col">    
  

      <form
        onSubmit={handleSubmit}
        className="mt-10 w-full max-w-2xl flex flex-col gap-7 glassmorphism"
      >
        <label>
          <span className="font-satoshi font-semibold text-base text-gray-700">
            Job Title
          </span>
          <input
            value={jobTitle}
            onChange={handleJobTitleChange}
            type="text"
            placeholder="Enter job title"
            required
            className="form_input"
          />
        </label>
        <label>
          <span className="font-satoshi font-semibold text-base text-gray-700">
            Company
          </span>
          <input
            value={company}
            onChange={handleCompanyChange}
            type="text"
            placeholder="Enter company name"
            required
            disabled={true}
            className="form_input"
          />
        </label>

        <label>
          <span className="font-satoshi font-semibold text-base text-gray-700">
            Work Place
          </span>
          <select
            value={workplace}
            onChange={handleWorkplaceChange}
            required
            className="form_input"
          >
            <option value="" disabled>Select work place</option>
              <option key={"Office"} value={"Office"}>{"Office"}</option>
              <option key={"Hybrid"} value={"Hybrid"}>{"Hybrid"}</option>
              <option key={"Remote"} value={"Remote"}>{"Remote"}</option>
          </select>
        </label>

        <label>
          <span className="font-satoshi font-semibold text-base text-gray-700">
            Location
          </span>
          <input
            value={location}
            onChange={handleLocationChange}
            type="text"
            placeholder="Enter location"
            required
            className="form_input"
          />
        </label>

        <label>
          <span className="font-satoshi font-semibold text-base text-gray-700">
            Job Type
          </span>
          <select
            value={jobType}
            onChange={handleJobTypeChange}
            required
            className="form_input"
          >
            <option value="" disabled>Select job type</option>
              <option key={"Full Time"} value={"Full Time"}>{"Full time"}</option>
              <option key={"Part Time"} value={"Part Time"}>{"Part time"}</option>
              <option key={"Internship"} value={"Internship"}>{"Internship"}</option>
          </select>
        </label>

      
        <div className="relative w-full min-w-[200px]">
          <textarea
            className="peer h-full min-h-[150px] w-full resize-none rounded-md border border-blue-gray-200 bg-white min-w-[210px] px-3 py-3 font-sans text-sm font-normal text-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-gray-900 focus:border-t-transparent focus:outline-0 disabled:resize-none disabled:border-0 disabled:bg-blue-gray-50"
            placeholder=" "
            value={requirements}
            onChange={handleRequirementsChange}
          ></textarea>
          <label className="before:content[' '] after:content[' ']  pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-gray-900 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:border-gray-900 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:border-gray-900 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
          Requirements
          </label>
        </div>

    
        <div className="relative w-full min-w-[200px]">
          <textarea
            className="peer h-full min-h-[150px] w-full resize-none rounded-md border border-blue-gray-200 bg-white min-w-[210px] px-3 py-3 font-sans text-sm font-normal text-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-gray-900 focus:border-t-transparent focus:outline-0 disabled:resize-none disabled:border-0 disabled:bg-blue-gray-50"
            placeholder=" "
            value={description}
            onChange={handleDescriptionChange}
          ></textarea>
          <label className="before:content[' '] after:content[' ']  pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-gray-900 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:border-gray-900 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:border-gray-900 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
          Description
          </label>
        </div>

       
        <div className="relative w-full min-w-[200px]">
          <textarea
            className="peer h-full min-h-[20px] w-full resize-none rounded-md border border-blue-gray-200 bg-white min-w-[210px] px-3 py-3 font-sans text-sm font-normal text-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-gray-900 focus:border-t-transparent focus:outline-0 disabled:resize-none disabled:border-0 disabled:bg-blue-gray-50"
            placeholder=" "
            value={salaryRange}
            onChange={handleSalaryRangeChange}
          ></textarea>
          <label className="before:content[' '] after:content[' ']  pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-gray-900 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:border-gray-900 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:border-gray-900 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
            Salary Range
          </label>
        </div>

        <div className="flex-end mx-3 mb-5 gap-4">
          <Link href="/" className="text-gray-500 text-sm">
            Cancel
          </Link>
          <button
            type="submit"
            disabled={submitting}
            className="px-5 py-1.5 text-sm bg-primary-orange rounded-full text-white"
          >
            {submitting ? `${type}ing...` : type}
          </button>
        </div>
      </form>
    </section>
  );
};

export default Form;
