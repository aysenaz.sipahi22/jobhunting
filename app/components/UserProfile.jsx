import React from 'react';

export const UserProfile = ({ user }) => {
  return (
    <div className="">
      <div className="bg-gray-100">
        <div className="container mx-auto py-8">
          <div className="grid grid-cols-4 sm:grid-cols-12 gap-6 px-4">
            <div className="col-span-4 sm:col-span-3">
              <div className="bg-white shadow rounded-lg p-6">
                <div className="flex flex-col items-center">
                  <img src={user.image} className="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0" />
                  <h1 className="text-xl font-bold">{user.name}</h1>
                  <p className="text-gray-700">{user.profession}</p>
                  <div className="mt-6 flex flex-wrap gap-4 justify-center">
                    {/* <a href="#" className="bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded">Contact</a> */}
                    {/* <a href="#" className="bg-gray-300 hover:bg-gray-400 text-gray-700 py-2 px-4 rounded">Resume</a> */}
                   
                    <h1>{user.email}</h1>
                    <h1>{user.phoneNumber}</h1>
                    <h1>{user.dob}</h1>
                    <h1>{user.address}</h1>
                    
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-4 sm:col-span-9">
              <div className="bg-white shadow rounded-lg p-6">
                <h2 className="text-xl font-bold mb-4">About Me</h2>
                <p className="text-gray-700">{user.aboutMe}
                </p>
                <h2 className="text-xl font-bold mb-4">Education</h2>
                <p className="text-gray-700">{user.education}
                </p>
                <h2 className="text-xl font-bold mb-4">Experience</h2>
                <p className="text-gray-700">{user.experience}
                </p>

    
        
              
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
