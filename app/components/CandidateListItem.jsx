'use client'
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import BusinessCenterIcon from '@mui/icons-material/BusinessCenter';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import PaidIcon from '@mui/icons-material/Paid';

const CandidateListItem = ({ candidate }) => {
  const { data: session } = useSession();
  const [isFavorited, setIsFavorited] = useState();
  const [isApplied, setIsApplied] = useState();
  const  postId  = window.location.pathname.split('/')[2]

  
  




  return (
    <div className="relative flex flex-col text-gray-700 bg-white shadow-md w-[600px] rounded-xl bg-clip-border mb-10">
      <nav className="flex min-w-[240px] flex-col gap-1 p-2 font-sans text-base font-normal text-blue-gray-700">
        <div className="flex items-center w-full p-3 leading-tight transition-all rounded-lg outline-none text-start hover:bg-blue-gray-50 hover:bg-opacity-80 hover:text-blue-gray-900 focus:bg-blue-gray-50 focus:bg-opacity-80 focus:text-blue-gray-900 active:bg-blue-gray-50 active:bg-opacity-80 active:text-blue-gray-900">
          <div className="grid mr-4 place-items-center">
          </div>
          <div className=" w-11/12">
            <div className="flex-between">
              <h6 className="block font-sans text-base antialiased font-semibold leading-relaxed tracking-normal text-blue-gray-900">
                {`post.jobTitle`}
              </h6>
              
            </div>
            <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
             
            </p>
            <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
              <LocationOnIcon />
              {/* {post.location} */}
            </p>
            <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
              <BusinessCenterIcon />
              {/* {post.workPlace + " - " + post.jobType} */}
            </p>
            <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
              {/* {post.salaryRange != "" ? <PaidIcon /> : ""}
              {post.salaryRange} */}
            </p>
          </div>
        </div>
      
      </nav>
    </div>
  );
};

export default CandidateListItem;
