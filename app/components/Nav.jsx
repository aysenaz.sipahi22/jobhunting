"use client";
import React, { useEffect, useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { signIn, signOut, useSession, getProviders } from "next-auth/react";
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import PostAddIcon from '@mui/icons-material/PostAdd';
import BookmarksIcon from '@mui/icons-material/Bookmarks';
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';
import InventoryIcon from '@mui/icons-material/Inventory';

const Nav = () => {
  const { data: session } = useSession();

  const [providers, setProviders] = useState(null);
  const [toggleDrowdown, setToggleDropdown] = useState(false);
  const [user, setUser] = useState("")
  useEffect(() => {
    const setUpProviders = async () => {
      const response = await getProviders();
      setProviders(response);
    };
    setUpProviders();

    getUserData(session?.user.email)
  }, []);


  useEffect(() => {
    getUserData(session?.user.email)
  
  }, [session])
  
  const getUserData = async (email) => {
    try {
      const response = await fetch('/api/user', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email }),
      });
      if (response.ok) {
        const userData = await response.json();
        setUser(userData);
      } else {
        setUser("");
      }
    } catch (error) {
      setUser("");
    }
  };


  return (
    <nav className="flex-between w-full mt-2 mb-6 p-3 bg-white shadow-md rounded-xl bg-clip-border" >
      <Link href="/" className="flex gap-2 flex-center">
        <Image
          src="/images/jobHuntingLogo.png"
          alt="job hunter"
          width={40}
          height={40}
          className="object-contain"
        ></Image>
        <p className="logo_text">Job Hunter</p>
      </Link>

      <div className="sm:flex hidden" style={{zIndex:"1000"}}>
        {session?.user ? (
          <div className="flex">
            <Image
              src={session?.user.image}
              alt="job hunter"
              width={40}
              height={40}
              className="object-contain"
              onClick={() => {
                setToggleDropdown((prev) => !prev);
              }}
              style={{ borderRadius: "50px", cursor: "pointer" }}
            ></Image>
            {toggleDrowdown && (
              <div className="flex flex-col dropdown">
                
                <Link
                  href="/profile"
                  className="dropdown_link"
                  onClick={() => setToggleDropdown(false)}
                >
                 <AccountBoxIcon/> My Profile
                </Link>
                {user && user.role == "employer" && 
                 <Link
                href="/new-post"
                className="dropdown_link"
                onClick={() => setToggleDropdown(false)}
                
              >
                <PostAddIcon/> New Post
              </Link>
                }
                 {user && user.role == "employer" && 
                 <Link
                href="/my-posts"
                className="dropdown_link"
                onClick={() => setToggleDropdown(false)}
                
              >
                <InventoryIcon/> My Posts
              </Link>
                }
               
               <Link
                  href="/favorites"
                  className="dropdown_link"
                  onClick={() => setToggleDropdown(false)}
                >
                 <BookmarksIcon/> Favorites
                </Link>
                
                <Link
                  href="/my-applications"
                  className="dropdown_link"
                  onClick={() => setToggleDropdown(false)}
                >
                 <FormatListBulletedIcon/> My Applications
                </Link>
                <button
                  type="button"
                  onClick={() => {
                    setToggleDropdown(false); // Close the dropdown
                    signOut(); // Sign out the user
                    window.location.href("localhost:3000"); // Sign out the user
                  }}
                  className="mt-5 w-full black_btn"
                >
                  Sign Out
                </button>
              </div>
            )}
          </div>
        ) : (
          <>
            {providers &&
              Object.values(providers).map((provider) => (
                <button
                  type="button"
                  key={provider.name}
                  onClick={() => signIn(provider.id)}
                  className="black_btn"
                >
                  Sign In
                </button>
              ))}
          </>
        )}
      </div>
      <div className="sm:hidden flex" style={{zIndex:"1000"}}>
        {session?.user ? (
          <div className="flex">
            <Image
              src={session?.user.image}
              alt="job hunter"
              width={40}
              height={40}
              className="object-contain"
              onClick={() => {
                setToggleDropdown((prev) => !prev);
              }}
              style={{ borderRadius: "50px" }}
            ></Image>
            {toggleDrowdown && (
              <div className="dropdownMobile">
                <Link
                  href="/profile"
                  className="dropdown_link"
                  onClick={() => setToggleDropdown(false)}
                >
                 <AccountBoxIcon/> My Profile
                </Link>
                {user && user.role == "employer" && 
                <Link
                  href="/new-post"
                  className="dropdown_link"
                  onClick={() => setToggleDropdown(false)}
                >
                  <PostAddIcon/> New Post
                </Link>
                }
                  {user && user.role == "employer" && 
                 <Link
                href="/my-posts"
                className="dropdown_link"
                onClick={() => setToggleDropdown(false)}
                
              >
                <InventoryIcon/> My Posts
              </Link>
                }
                 <Link
                  href="/favorites"
                  className="dropdown_link"
                  onClick={() => setToggleDropdown(false)}
                >
                 <BookmarksIcon/> Favorites
                </Link>
                <Link
                  href="/my-applications"
                  className="dropdown_link"
                  onClick={() => setToggleDropdown(false)}
                >
                 <FormatListBulletedIcon/> My Applications
                </Link>
                <button
                  type="button"
                  onClick={() => {
                    setToggleDropdown(false); // Close the dropdown
                    signOut();
                  }}
                  className="mt-5 w-full black_btn"
                >
                  Sign Out
                </button>
              </div>
            )}
          </div>
        ) : (
          <>
            {providers &&
              Object.values(providers).map((provider) => (
                <button
                  type="button"
                  key={provider.name}
                  onClick={() => signIn(provider.id)}
                  className="black_btn"
                >Sign In</button>
              ))}
          </>
        )}
      </div>
    </nav>
  );
};

export default Nav;
