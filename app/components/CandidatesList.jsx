'use client'
import Link from 'next/link';
import React, { useEffect, useState } from 'react';

const CandidatesList = ({ candidates }) => {
  const [appliers, setAppliers] = useState()
useEffect(() => {
  if(candidates){
    setAppliers(candidates)
  }
}, [candidates])


  
  return (
    <div className="relative flex flex-col text-gray-700 bg-white shadow-md w-[600px] rounded-xl bg-clip-border mb-10">
      {appliers && appliers.map((candidate) => (
  <div key={candidate.id}>
    <nav className="flex min-w-[240px] flex-col gap-1 p-2 font-sans text-base font-normal text-blue-gray-700">
        <div className="flex items-center w-full p-3 leading-tight transition-all rounded-lg outline-none text-start hover:bg-blue-gray-50 hover:bg-opacity-80 hover:text-blue-gray-900 focus:bg-blue-gray-50 focus:bg-opacity-80 focus:text-blue-gray-900 active:bg-blue-gray-50 active:bg-opacity-80 active:text-blue-gray-900">
          <div className="grid mr-4 place-items-center">
            <Link href="/profile/[userId]" as={`/profile/${candidate.id}`}>
              <img
                alt="candice"
                src={candidate.image}
                className="relative inline-block h-12 w-12 !rounded-full  object-cover object-center"
              />
            </Link>
          </div>
          <div className=" w-11/12">
            <div className="flex-between">
              <h6 className="block font-sans text-base antialiased font-semibold leading-relaxed tracking-normal text-blue-gray-900">
              <Link
                href="/profile/[userId]"
                as={`/profile/${candidate.id}`}
              >
                {candidate.name}
              </Link>
              </h6>
              {/* {isFavorited ? (
                <BookmarkIcon
                  style={{ cursor: "pointer" }}
                  onClick={toggleFavorite}
                />
              ) : (
                <BookmarkBorderIcon
                  style={{ cursor: "pointer" }}
                  onClick={toggleFavorite}
                />
              )} */}
            </div>
            <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
             
                {candidate.profession}
            </p>
            {/* <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
              <LocationOnIcon />
              {post.location}
            </p>
            <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
              <BusinessCenterIcon />
              {post.workPlace + " - " + post.jobType}
            </p> */}
            <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
              {candidate.email}
            </p>
            <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
              {candidate.phoneNumber}
            </p>
            <p className="block font-sans text-sm antialiased font-normal leading-normal text-gray-700">
              {candidate.dob}
            </p>
          </div>
        </div>
        {/* <div className="flex-end">
          <Link
            href="/posts/[postId]"
            as={`/posts/${post.id}`}
            className="text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 shadow-lg shadow-purple-500/50 dark:shadow-lg dark:shadow-purple-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
          >
            See Details
          </Link>
          <button
            onClick={applyToPost}
            type="button"
            disabled={isApplied}
            className={`text-purple-700 ${
              isApplied? "bg-white-300 cursor-not-allowed border-solid border-2 border-purple-500 text-purple-700": "bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 shadow-lg shadow-purple-500/50 dark:shadow-lg dark:shadow-purple-800/80"} font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2`}
          >
            {isApplied ? "Applied" : "Apply"}
          </button>
        </div> */}
      </nav>
      <div className="flex-end">
          <Link
            href="/profile/[userId]"
            as={`/profile/${candidate.id}`}
            className="text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 shadow-lg shadow-purple-500/50 dark:shadow-lg dark:shadow-purple-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
          >
            Inspect
          </Link>
          
        </div>
    {/* Diğer alanları da benzer şekilde kontrol edebilirsiniz */}
  </div>
))}


      
    </div>
  );
};

export default CandidatesList;
