"use client";
// pages/posts/[postId]/page.jsx
import React, { useState, useEffect } from "react";
import { useParams } from "next/navigation";
import Link from "next/link";
import PostListItem from "../../components/PostListItem";
import { useSession } from "next-auth/react";

const PostPage = () => {
  const [company, setCompany] = useState(null);
  const [posts, setPosts] = useState();
  const [user, setUser] = useState();
  const { companyId } = useParams();


  const { data: session } = useSession();


  useEffect(() => {
    if (session) {
      getUserData(session.user.email);
    }
  }, [session]);



  const getUserData = async (email) => {
    try {
      const response = await fetch('/api/user', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email }),
      });
      if (response.ok) {
        const userData = await response.json();
        setUser(userData)

      } else {
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    async function fetchPost() {
      try {
        const response = await fetch(`/api/getCompanyById/${companyId}`, {
          method: "GET",
        });
        if (!response.ok) {
          throw new Error("Failed to fetch post");
        }
        const companyData = await response.json();
        setCompany(companyData);
      } catch (error) {
        console.error("Error:", error);
      }

      
    }

    fetchPost();
    getPostsData()
  }, [companyId]);

  const getPostsData = async () => {
    try {
      const response = await fetch('/api/getPosts', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-store',
        },
        cache:"no-store",
      });
      if (response.ok) {
        const postsData = await response.json();
        const companyPosts = postsData.filter(post => post && post.companyId && post.companyId.includes(companyId));
        setPosts(companyPosts);
      } else {
      }
    } catch (error) {
      console.error(error);
    }
  };

  

  if (!company) {
    return <div>Loading...</div>;
  }

  return (
      <div className="">
        <div className="bg-gray-100">
          <div className="container mx-auto py-8">
            <div className="grid grid-cols-4 sm:grid-cols-12 gap-6 px-4">
              <div className="col-span-4 sm:col-span-3">
                <div className="bg-white shadow rounded-lg p-6">
                  <div className="flex flex-col items-center">
                    <img src={company.image} className="w-32 h-32 bg-gray-300 rounded-full mb-4 shrink-0" />
                    <h1 className="text-xl font-bold"> <Link
                href="/company/[companyId]"
                as={`/company/${companyId}`}
              >
                {company.companyName}
              </Link> </h1>
                    <p className="text-gray-700">Software Developer</p>
                    <div className="mt-6 flex flex-wrap gap-4 justify-center">
                      {/* <a href="#" className="bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded">Contact</a> */}
                      {/* <a href="#" className="bg-gray-300 hover:bg-gray-400 text-gray-700 py-2 px-4 rounded">Resume</a> */}
                     
                      <h1>{company.email}</h1>
                      <h1>{company.phoneNumber}</h1>
                      <h1>{company.address}</h1>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-span-4 sm:col-span-9">
                <div className="bg-white shadow rounded-lg p-6 mb-4">
                  <h2 className="text-xl font-bold mb-4">About Me</h2>
                  <p className="text-gray-700">{company.companyDescription}
                  </p>               
                </div>
                {posts && posts.map((post) => (
        <PostListItem
        key = {post.id}
        post = {post}
        user = {user}
        />
      )       
      )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
};

export default PostPage;
